package com.deloitte.nycschools.di

/**
 * List of child modules
 */
val testApplicationModule = listOf(
    viewModelModule,
    servicesModule,
    repositoryModule,
    databaseModule,
)
