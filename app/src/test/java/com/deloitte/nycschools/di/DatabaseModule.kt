package com.deloitte.nycschools.di

import androidx.room.Room
import com.deloitte.nycschools.db.AppDatabase
import org.koin.dsl.module

val databaseModule = module {
    single {
        // Use an in memory DB for testing
        Room.inMemoryDatabaseBuilder(get(), AppDatabase::class.java)
            .allowMainThreadQueries()
            .build()
    }
    single { get<AppDatabase>().schoolDao() }
}
