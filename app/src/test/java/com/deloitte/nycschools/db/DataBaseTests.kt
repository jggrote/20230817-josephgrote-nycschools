package com.deloitte.nycschools.db

import com.deloitte.nycschools.BaseTest
import com.deloitte.nycschools.db.model.School
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.runner.RunWith
import org.koin.core.component.inject
import org.robolectric.RobolectricTestRunner
import kotlin.test.Test

@RunWith(RobolectricTestRunner::class)
class DataBaseTests : BaseTest() {
    val schoolDao: SchoolDao by inject()

    @Test
    fun insertSchoolExpectSchool() = runTest {
        var count = schoolDao.getSchoolCount()
        Assert.assertEquals(0, count)

        schoolDao.insertSchools(listOf(SCHOOL1))
        count = schoolDao.getSchoolCount()
        Assert.assertEquals(1, count)

        val schools = schoolDao.getSchools().first()
        Assert.assertEquals(SCHOOL1, schools[0])
    }

    companion object {
        val SCHOOL1 = School(
            id = "School1",
            name = "School One",
            address = "111 1st st",
            city = "NYC",
            zip = "10003",
            state = "NY",
            email = "",
            website = "",
            studentCount = 100,
        )
    }
}
