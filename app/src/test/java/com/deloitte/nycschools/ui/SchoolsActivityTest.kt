package com.deloitte.nycschools.ui

import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import com.deloitte.nycschools.BaseTest
import com.deloitte.nycschools.R
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(RobolectricTestRunner::class)
class SchoolsActivityTest : BaseTest() {
    @Test
    fun testSchoolsActivity_fragmentsShown() = runTest {
        val scenario = ActivityScenario.launch(SchoolsActivity::class.java)
        scenario.onActivity { activity ->
            val navHost =
                activity.supportFragmentManager.findFragmentById(R.id.nav_host_fragment)!!
            val fragment = navHost.childFragmentManager.fragments[0]
            Assert.assertTrue(fragment is SchoolsFragment)

            val schools = fragment!!.view!!.findViewById<RecyclerView>(R.id.schools)

            // Needed to get the RecyclerView to build its children
            flushSchedular()

            val holder = schools.findViewHolderForAdapterPosition(1)!!
            holder.itemView.performClick()
            navHost.childFragmentManager.executePendingTransactions()

            val detailsFragment = navHost.childFragmentManager.fragments[0]
            Assert.assertTrue(detailsFragment is SchoolDetailsFragment)
            flushSchedular()
        }
        scenario.close()
    }

    fun TestScope.flushSchedular() {
        advanceUntilIdle()
        Robolectric.flushForegroundThreadScheduler()
    }
}
