package com.deloitte.nycschools

import android.app.Application
import com.deloitte.nycschools.di.testApplicationModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin

class TestSchoolsApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        stopKoin()
        startKoin {
            androidContext(this@TestSchoolsApplication)
            modules(testApplicationModule)
        }
    }
}
