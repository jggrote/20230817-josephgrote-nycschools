package com.deloitte.nycschools

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.withTransaction
import androidx.test.core.app.ApplicationProvider
import com.deloitte.nycschools.db.AppDatabase
import io.mockk.coEvery
import io.mockk.mockkStatic
import io.mockk.slot
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.setMain
import org.junit.Rule
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import org.robolectric.annotation.Config

@Config(application = TestSchoolsApplication::class)
open class BaseTest : KoinComponent {
    val context = ApplicationProvider.getApplicationContext<Context>()
    val appDatabase: AppDatabase by inject()
    val transactionLambda = slot<suspend () -> R>()

    init {
        Dispatchers.setMain(TestCoroutineDispatcher())
        mockTransactions()
    }

    fun mockTransactions() {
        mockkStatic(
            "androidx.room.RoomDatabaseKt"
        )

        // Needed to work around a bug where Robolectric hangs on Room transactions.
        coEvery { appDatabase.withTransaction(capture(transactionLambda)) } coAnswers {
            transactionLambda.captured.invoke()
        }
    }

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()
}
