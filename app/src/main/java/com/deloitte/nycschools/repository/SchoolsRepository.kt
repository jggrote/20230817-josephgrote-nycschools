package com.deloitte.nycschools.repository

import android.content.Context
import com.deloitte.nycschools.db.SchoolDao
import com.deloitte.nycschools.db.model.SATScores
import com.deloitte.nycschools.db.model.School
import com.deloitte.nycschools.service.RetrofitServices
import com.deloitte.nycschools.service.model.SATScoresAPI
import com.deloitte.nycschools.service.model.SchoolAPI
import com.deloitte.nycschools.util.readAssetFile
import kotlinx.coroutines.flow.Flow
import kotlinx.serialization.json.Json
import okio.IOException
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class SchoolsRepository(val context: Context) : KoinComponent {
    val schoolDao: SchoolDao by inject()
    val services: RetrofitServices by inject()
    val schools: Flow<List<School>> = schoolDao.getSchools()

    suspend fun getSchool(id: String): School? = schoolDao.getSchool(id)
    suspend fun getSATScores(id: String): SATScores? = schoolDao.getScores(id)

    /**
     * Load the schools from asset files
     */
    suspend fun initialize() {
        if (schoolDao.getSchoolCount() > 0) return

        loadSchoolsFromAssets()
//        loadSchoolsFromService()
    }

    private suspend fun loadSchoolsFromAssets() {
        val json = Json { ignoreUnknownKeys = true }

        val schoolJSON = context.readAssetFile("NYCSchools.json") ?: return
        val schoolsAPI = json.decodeFromString<List<SchoolAPI>>(schoolJSON)
        val schools = schoolsAPI.map { it.toSchool() }
        schoolDao.insertSchools(schools)

        val satJSON = context.readAssetFile("NYCSAT.json") ?: return
        val satAPI = json.decodeFromString<List<SATScoresAPI>>(satJSON)
        val scores = satAPI.map { it.toSAT() }
        schoolDao.insertSATScores(scores)
    }

    private suspend fun loadSchoolsFromService() {
        val json = Json { ignoreUnknownKeys = true }

        val response = services.schoolsService.getSchools()

        val schoolsAPI = if (response.isSuccessful) {
            response.body() ?: emptyList()
        } else {
            throw IOException("Error loading schools")
        }

        val schools = schoolsAPI.map { it.toSchool() }
        schoolDao.insertSchools(schools)

        val satResponse = services.schoolsService.getSATScores()

        val satAPI = if (satResponse.isSuccessful) {
            satResponse.body() ?: emptyList()
        } else {
            throw IOException("Error loading SAT scores")
        }

        val scores = satAPI.map { it.toSAT() }
        schoolDao.insertSATScores(scores)
    }
}

fun SchoolAPI.toSchool() = School(
    id = dbn,
    name = school_name,
    address = primary_address_line_1 ?: "",
    city = city ?: "",
    zip = zip ?: "",
    state = state_code ?: "",
    email = school_email ?: "",
    website = website ?: "",
    studentCount = total_students.parseInt(),
)

fun SATScoresAPI.toSAT() = SATScores(
    id = dbn,
    testTakerCount = num_of_sat_test_takers.parseInt(),
    readingScore = sat_critical_reading_avg_score.parseInt(),
    mathScore = sat_math_avg_score.parseInt(),
    writingScore = sat_writing_avg_score.parseInt()
)

fun String?.parseInt() =
    try {
        this?.toInt()
    } catch (e: NumberFormatException) {
        0
    } ?: 0
