package com.deloitte.nycschools.di

import androidx.room.Room
import com.deloitte.nycschools.db.AppDatabase
import org.koin.dsl.module

val databaseModule = module {
    single { Room.databaseBuilder(get(), AppDatabase::class.java, "nycschools").build() }
    single { get<AppDatabase>().schoolDao() }
}
