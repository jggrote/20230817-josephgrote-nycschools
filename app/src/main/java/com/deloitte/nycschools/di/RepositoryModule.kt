package com.deloitte.nycschools.di

import com.deloitte.nycschools.repository.SchoolsRepository
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val repositoryModule = module {
    single { SchoolsRepository(androidApplication()) }
}
