package com.deloitte.nycschools.di

import com.deloitte.nycschools.ui.SchoolsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Module to inject all the ViewModels
 */
val viewModelModule = module {
    viewModel { SchoolsViewModel(get()) }
}
