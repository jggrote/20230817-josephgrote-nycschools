package com.deloitte.nycschools.di

import com.deloitte.nycschools.service.RetrofitServices
import org.koin.dsl.module

/**
 * Module to inject all services
 */
val servicesModule = module {
    single { RetrofitServices() }
}
