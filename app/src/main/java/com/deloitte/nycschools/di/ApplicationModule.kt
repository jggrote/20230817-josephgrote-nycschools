package com.deloitte.nycschools.di

/**
 * List of child modules
 */
val applicationModule = listOf(
    viewModelModule,
    servicesModule,
    repositoryModule,
    databaseModule,
)
