package com.deloitte.nycschools.ui

import androidx.lifecycle.ViewModel
import com.deloitte.nycschools.db.model.SATScores
import com.deloitte.nycschools.db.model.School
import com.deloitte.nycschools.repository.SchoolsRepository
import kotlinx.coroutines.flow.Flow

class SchoolsViewModel(private val repository: SchoolsRepository) : ViewModel() {
    val schools: Flow<List<School>> = repository.schools

    suspend fun getSchool(id: String): School? = repository.getSchool(id)
    suspend fun getSATScores(id: String): SATScores? = repository.getSATScores(id)
}
