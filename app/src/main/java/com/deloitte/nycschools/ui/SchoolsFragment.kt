package com.deloitte.nycschools.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.deloitte.nycschools.R
import com.deloitte.nycschools.databinding.SchoolItemBinding
import com.deloitte.nycschools.databinding.SchoolsFragmentBinding
import com.deloitte.nycschools.db.model.School
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.activityViewModel

class SchoolsFragment : Fragment() {
    private lateinit var binding: SchoolsFragmentBinding
    private val viewModel: SchoolsViewModel by activityViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = SchoolsFragmentBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.schools.adapter = SchoolAdapter()
        setListeners()
    }

    override fun onResume() {
        super.onResume()
        val actionbar = (requireActivity() as? AppCompatActivity)?.supportActionBar
        actionbar?.setTitle(R.string.schools_title)
    }

    fun onSchoolClicked(school: School) {
        val directions = SchoolsFragmentDirections.actionSchoolsToSchoolDetails(school.id)
        findNavController().navigate(directions)
    }

    private fun setListeners() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.schools.catch {
                    Toast.makeText(
                        requireContext(),
                        "Error loading data: ${it.message}",
                        Toast.LENGTH_LONG
                    ).show()
                }.collect { schools ->
                    (binding.schools.adapter as SchoolAdapter).submitList(schools)
                    binding.progress.isVisible = false
                    binding.empty.isVisible = schools.isEmpty()
                }
            }
        }
    }

    inner class SchoolAdapter : ListAdapter<School, SchoolViewHolder>(SchoolDiffCallBack()) {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = SchoolItemBinding.inflate(layoutInflater, parent, false)
            return SchoolViewHolder(binding)
        }

        override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) {
            holder.bind(getItem(position))
        }
    }

    inner class SchoolViewHolder(val binding: SchoolItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(school: School) {
            binding.name.text = school.name

            binding.root.setOnClickListener {
                onSchoolClicked(school)
            }
        }
    }

    class SchoolDiffCallBack : DiffUtil.ItemCallback<School>() {
        override fun areItemsTheSame(oldItem: School, newItem: School): Boolean {
            return oldItem.name == newItem.name
        }

        override fun areContentsTheSame(oldItem: School, newItem: School): Boolean {
            return oldItem == newItem
        }
    }
}
