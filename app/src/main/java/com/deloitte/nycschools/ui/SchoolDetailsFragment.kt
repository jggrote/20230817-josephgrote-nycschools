package com.deloitte.nycschools.ui

import android.os.Bundle
import android.text.util.Linkify
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.deloitte.nycschools.R
import com.deloitte.nycschools.databinding.SchoolDetailsFragmentBinding
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.activityViewModel

class SchoolDetailsFragment : Fragment() {
    private lateinit var binding: SchoolDetailsFragmentBinding
    private val viewModel: SchoolsViewModel by activityViewModel()
    private val args: SchoolDetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = SchoolDetailsFragmentBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        displaySchool()
    }

    override fun onResume() {
        super.onResume()

        val actionbar = (requireActivity() as? AppCompatActivity)?.supportActionBar
        actionbar?.setTitle(R.string.school_details_title)
        actionbar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun displaySchool() {
        lifecycleScope.launch {
            viewModel.getSchool(args.schoolId)?.let { school ->
                binding.name.text = school.name
                binding.address1.text = school.address
                binding.address2.text =
                    getString(R.string.school_address, school.city, school.state, school.zip)
                binding.web.text = school.website
                Linkify.addLinks(binding.web, Linkify.WEB_URLS)
            }

            viewModel.getSATScores(args.schoolId)?.let { scores ->
                binding.reading.text = getString(R.string.reading_score, scores.readingScore)
                binding.writing.text = getString(R.string.writing_score, scores.writingScore)
                binding.math.text = getString(R.string.math_score, scores.mathScore)
            }
        }
    }
}
