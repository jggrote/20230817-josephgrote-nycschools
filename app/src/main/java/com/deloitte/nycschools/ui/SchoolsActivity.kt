package com.deloitte.nycschools.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.deloitte.nycschools.R
import com.deloitte.nycschools.databinding.SchoolsActivityBinding
import com.deloitte.nycschools.repository.SchoolsRepository
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class SchoolsActivity : AppCompatActivity() {
    val repository: SchoolsRepository by inject()
    private lateinit var binding: SchoolsActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Create the view binding and set the content and configure the action bar
        binding = SchoolsActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val navController = navHostFragment.navController
        val appBarConfiguration = AppBarConfiguration(navController.graph)
        binding.toolbar.setupWithNavController(
            navController,
            appBarConfiguration
        )

        fetchData()
    }

    fun fetchData() {
        lifecycleScope.launch {
            repository.initialize()
        }
    }
}
