package com.deloitte.nycschools.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.deloitte.nycschools.db.model.SATScores
import com.deloitte.nycschools.db.model.School

@Database(
    entities = [School::class, SATScores::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun schoolDao(): SchoolDao
}
