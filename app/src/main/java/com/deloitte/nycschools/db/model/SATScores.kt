package com.deloitte.nycschools.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class SATScores(
    @PrimaryKey
    val id: String,
    val testTakerCount: Int,
    val readingScore: Int,
    val mathScore: Int,
    val writingScore: Int,
)
