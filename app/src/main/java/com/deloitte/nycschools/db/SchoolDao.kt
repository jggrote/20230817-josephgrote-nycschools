package com.deloitte.nycschools.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.deloitte.nycschools.db.model.SATScores
import com.deloitte.nycschools.db.model.School
import kotlinx.coroutines.flow.Flow

@Dao
interface SchoolDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSchools(schools: List<School>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSATScores(scores: List<SATScores>)

    @Query("SELECT COUNT(*) FROM School")
    suspend fun getSchoolCount(): Int

    @Query("SELECT * FROM School ORDER BY name ASC")
    fun getSchools(): Flow<List<School>>

    @Query("SELECT * FROM School WHERE :schoolId == id")
    suspend fun getSchool(schoolId: String): School?

    @Query("SELECT * FROM SATScores WHERE :schoolId == id")
    suspend fun getScores(schoolId: String): SATScores?
}
