package com.deloitte.nycschools.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class School(
    @PrimaryKey
    val id: String,
    val name: String,
    val address: String,
    val city: String,
    val zip: String,
    val state: String,
    val email: String,
    val website: String,
    val studentCount: Int,
)
