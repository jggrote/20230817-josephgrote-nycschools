package com.deloitte.nycschools.service

import com.deloitte.nycschools.service.model.SATScoresAPI
import com.deloitte.nycschools.service.model.SchoolAPI
import retrofit2.Response
import retrofit2.http.GET

interface NYCSchoolsService {
    @GET("resource/s3k6-pzi2.json")
    suspend fun getSchools(): Response<List<SchoolAPI>>

    @GET("resource/f9bf-2cp4.json")
    suspend fun getSATScores(): Response<List<SATScoresAPI>>
}
