package com.deloitte.nycschools.service.model

import kotlinx.serialization.Serializable

@Serializable
class SATScoresAPI(
    val dbn: String,
    val num_of_sat_test_takers: String? = null,
    val sat_critical_reading_avg_score: String? = null,
    val sat_math_avg_score: String? = null,
    val sat_writing_avg_score: String? = null,
)
