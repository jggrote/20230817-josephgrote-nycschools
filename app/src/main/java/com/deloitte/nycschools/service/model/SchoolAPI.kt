package com.deloitte.nycschools.service.model

import kotlinx.serialization.Serializable

@Serializable
class SchoolAPI(
    val dbn: String,
    val school_name: String,
    val primary_address_line_1: String? = null,
    val city: String? = null,
    val zip: String? = null,
    val state_code: String? = null,
    val school_email: String? = null,
    val website: String? = null,
    val total_students: String? = null,
)
