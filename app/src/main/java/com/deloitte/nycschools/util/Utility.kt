package com.deloitte.nycschools.util

import android.content.Context
import android.util.Log
import java.io.IOException

/**
 * Load an assets file into a String
 * returns null if not found
 */
fun Context.readAssetFile(fileName: String): String? {
    return try {
        val inputStream = assets.open(fileName)

        val buffer = inputStream.use {
            inputStream.readBytes()
        }
        return String(buffer)
    } catch (e: IOException) {
        Log.e("Assets", "Error loading asset: ${e.message}")
        null
    }
}
