package com.deloitte.nycschools

import android.app.Application
import com.deloitte.nycschools.di.applicationModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.component.KoinComponent
import org.koin.core.context.startKoin

class NYCSchoolsApplication : Application(), KoinComponent {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@NYCSchoolsApplication)
            modules(applicationModule)
        }
    }
}
